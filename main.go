package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Build struct {
	Runtime   string `json:"runtime_seconds"`
	BuildDate string `json:"build_date"`
	Result    string `json:"result"`
	Output    string `json:"output"`
}

type BuildBase struct {
	Base []Build `json:"Builds"`
}

type Jobs struct {
	Job map[string]BuildBase `json:"jobs"`
}

type server struct{}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "get called"}`))
	case "POST":

		var v Jobs
		err := json.NewDecoder(r.Body).Decode(&v)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var tmpBuild Build

		for _, b := range v.Job["Build base AMI"].Base {
			if b.Runtime != "" && b.BuildDate != "" && b.Result != "" && b.Output != "" {
				t, err := strconv.Atoi(tmpBuild.BuildDate)
				if err != nil {
					t = 0
				}
				m, err := strconv.Atoi(b.BuildDate)
				if err != nil {
					panic(err)
				}
				if m > t {
					tmpBuild = b
				}

			}
		}

		output := strings.Fields(tmpBuild.Output)

		bt, _ := strconv.ParseInt(tmpBuild.BuildDate, 10, 64)
		buildDate := time.Unix(bt, 0)

		returnOutput := fmt.Sprintf("{\n    \"latest\":{\n        \"build_date\":\"%s\",\n        \"ami_id\":\"%s\",\n        \"commit_hash\":\"%s\"\n    }\n}", buildDate, output[2], output[3])

		fmt.Fprintf(w, "%+v", returnOutput)

	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "put called"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "delete called"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "not found"}`))
	}
}

func main() {
	s := &server{}
	http.Handle("/", s)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
